package com.wlebedev.weatherstationgui;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final String LOG_TAG = "myLogs";
    final String FILENAME = "file";
    final String DIR_SD = "MyFiles";
    final String FILENAME_SD = "fileSD";

    private final static String FILE_NAME = "content.txt";

    //Сокет, с помощью которого мы будем отправлять данные на Arduino
    BluetoothSocket clientSocket;

    Double globalTemp = 0.0;
    Double globalHumidity = 0.0;
    Double globalPressure = 0.0;
    Double globalHeight = 0.0;
    OutputStream outputStream = null;

    //Поля с единицами исмерения
    TextView codeTempLable;
    TextView codeHumidityLable;
    TextView codePressureLable;
    TextView codeHeightLable;

    // поля вывода
    TextView codeTempView;
    TextView codeHumidityView;
    TextView codePressureView;
    TextView codeHeightView;

    // кнопки
    Button codeTimeButton;

    //Поток для динамического обновления данных
    // ну это на самом деле не поток вроде так, а тп залупа такая с помощью которой можно из друго метода (не create)
    // изменения отображения
    Handler tempViewChanger;
    Handler humidityViewChanger;
    Handler pressureViewChanger;
    Handler heightViewChanger;
    Handler tempChanger;
    Handler humidityChanger;
    Handler pressureChanger;
    Handler heightChanger;

    Boolean flagTemp;
    Boolean flagHumidity;
    Boolean flagPressure;
    Boolean flagHeight;

    // КОНСТАННТЫ
    private static final String TEXT_TEMP = "Температура в °С";
    private static final String TEXT_HUMIITY = "Влажность в %";
    private static final String TEXT_PRESSURE = "Давление в мм рт";
    private static final String TEXT_HEIGHT = "Высота в м";

    private static final String TEXT_TEMP_ALTERNNATIVA = "Температура в °F";
    private static final String TEXT_HUMIITY_ALTERNNATIVA = "Влажность";
    private static final String TEXT_PRESSURE_ALTERNNATIVA = "Давление в Pa";
    private static final String TEXT_HEIGHT_ALTERNNATIVA = "Высота в см";

    private static final String TEXT_HUMIITY_RELATIVE_MMEASURE_FEW_PIZDEC = "оч низкая";  // 0%  - 20%
    private static final String TEXT_HUMIITY_RELATIVE_MMEASURE_FEW = "низкая";               // 20% - 40%
    private static final String TEXT_HUMIITY_RELATIVE_MMEASURE_MEDIUM = "среняя";            // 40% - 60%
    private static final String TEXT_HUMIITY_RELATIVE_MMEASURE_HARD = "высокая";             // 60% - 80%
    private static final String TEXT_HUMIITY_RELATIVE_MMEASURE_HARD_PIZDEC = "оч высокая";// 80% - 100%

    private static final double ZHENYA_HEIGHT = 0.01;



    private void inicialize ()
    {


        flagTemp = true;
        flagHumidity = true;
        flagPressure = true;
        flagHeight = true;

        codeTempLable.setText("Температура в °С");
        codeHumidityLable.setText("Влажность в %");
        codePressureLable.setText("Давление в Pa");
        codeHeightLable.setText("Высота в м");
    }

    // функции для перевода или не перевода в другие еденици измерения
    // градусы здорового человека / градусы курильщика
    public String tempChangeUnit (boolean flag, Double temp)
    {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(flag ? temp : temp * 1.8 + 32 ) + "";
    }

    // проценты / слова
    public String humidityChangeUnit (boolean flag, Double humidity)
    {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        String result = "жёпа";
        if (flag) {return df.format(humidity) + "";}
        if ( humidity <= 20 ){return TEXT_HUMIITY_RELATIVE_MMEASURE_FEW_PIZDEC;}
        if ( humidity <= 40 ){return TEXT_HUMIITY_RELATIVE_MMEASURE_FEW;}
        if ( humidity <= 60 ){return TEXT_HUMIITY_RELATIVE_MMEASURE_MEDIUM;}
        if ( humidity <= 80 ){return TEXT_HUMIITY_RELATIVE_MMEASURE_HARD;}
        if ( humidity <= 100){return TEXT_HUMIITY_RELATIVE_MMEASURE_HARD_PIZDEC;}
        return result;
    }

    // мм рт / паскалИ
    public String pressureChangeUnit (boolean flag, Double pressure)
    {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(flag ? pressure : pressure * 133.322 ) + "";
    }

    // метры / монобровые карлики (метры курильщика)
    public String heightChangeUnit (boolean flag, Double height)
    {
        DecimalFormat df = new DecimalFormat("#.####");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(flag ? height : height / 1.7) + "";
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //"Соединям" реализации и элементы
        codeTempLable = (TextView) findViewById(R.id.tempLable);
        codeHumidityLable = (TextView) findViewById(R.id.humidityLable);
        codePressureLable = (TextView) findViewById(R.id.pressureLable);
        codeHeightLable = (TextView) findViewById(R.id.heightLable);

        codeTempView = (TextView) findViewById(R.id.tempView);
        codeHumidityView = (TextView) findViewById(R.id.humidityView);
        codePressureView = (TextView) findViewById(R.id.pressureView);
        codeHeightView = (TextView) findViewById(R.id.heaghtView);

        codeTimeButton = (Button) findViewById(R.id.button2);

        codeTempLable.setOnClickListener(this);
        codeHumidityLable.setOnClickListener(this);
        codePressureLable.setOnClickListener(this);
        codeHeightLable.setOnClickListener(this);
        codeTimeButton.setOnClickListener(this);

        // инициализация
        this.inicialize();

        tempChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codeTempLable.setText((flagTemp ? TEXT_TEMP : TEXT_TEMP_ALTERNNATIVA));
            };
        };

        humidityChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codeHumidityLable.setText((flagHumidity ? TEXT_HUMIITY : TEXT_HUMIITY_ALTERNNATIVA));
            };
        };

        pressureChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codePressureLable.setText((flagPressure ? TEXT_PRESSURE : TEXT_PRESSURE_ALTERNNATIVA));
            };
        };

        heightChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codeHeightLable.setText((flagHeight ? TEXT_HEIGHT : TEXT_HEIGHT_ALTERNNATIVA));
            };
        };

        // функции для отображения динамически изменяемых данных
        tempViewChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codeTempView.setText(String.valueOf(msg.obj));
            };
        };

        humidityViewChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codeHumidityView.setText(String.valueOf(msg.obj));
            };
        };

        pressureViewChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codePressureView.setText(String.valueOf(msg.obj));
            };
        };

        heightViewChanger = new Handler() {
            public void handleMessage(android.os.Message msg) {
                codeHeightView.setText(String.valueOf(msg.obj));
            };
        };

        tempViewChanger.obtainMessage(1, tempChangeUnit(flagTemp, globalTemp)).sendToTarget();
        humidityViewChanger.obtainMessage(1, humidityChangeUnit(flagHumidity, globalHumidity)).sendToTarget();
        pressureViewChanger.obtainMessage(1, pressureChangeUnit(flagPressure, globalPressure)).sendToTarget();
        heightViewChanger.obtainMessage(1, heightChangeUnit(flagHeight, globalHeight)).sendToTarget();

        // эмулятор---------------------------------------------------------------------------------
        emulator ();
        // эмулятор---------------------------------------------------------------------------------

        // херня с блютузом-------------------------------------------------------------------------
        //hernyaWithBluetooth();
        // херня с блютузом-------------------------------------------------------------------------
    }

    private void emulator () {
        Thread arduinoPriemnikSimulator = new Thread(new Runnable() {
            public void run() {

                String fileName = "Log.txt";

                File file = createLogFile(fileName);

                while (true) {
                    globalTemp = 24.0;
                    globalHumidity = 100.0;
                    globalPressure = 50.0;
                    globalHeight = -45.0;

                    tempViewChanger.obtainMessage(1, tempChangeUnit(flagTemp, globalTemp)).sendToTarget();
                    humidityViewChanger.obtainMessage(1, humidityChangeUnit(flagHumidity, globalHumidity)).sendToTarget();
                    pressureViewChanger.obtainMessage(1, pressureChangeUnit(flagPressure, globalPressure)).sendToTarget();
                    heightViewChanger.obtainMessage(1, heightChangeUnit(flagHeight, globalHeight)).sendToTarget();

                    String message = createLogString();
                    writeLog(file, message);
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        arduinoPriemnikSimulator.start();
    }

    private void hernyaWithBluetooth () {
        //Включаем bluetooth. Если он уже включен, то ничего не произойдет
        String enableBT = BluetoothAdapter.ACTION_REQUEST_ENABLE;
        startActivityForResult(new Intent(enableBT), 0);

        //Мы хотим использовать тот bluetooth-адаптер, который задается по умолчанию
        BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();

        //Пытаемся проделать эти действия
        try{

            BluetoothDevice device = bluetooth.getRemoteDevice("98:D3:21:F4:92:D5");

            //Инициируем соединение с устройством
            Method m = device.getClass().getMethod(
                    "createRfcommSocket", new Class[] {int.class});

            clientSocket = (BluetoothSocket) m.invoke(device, 1);
            clientSocket.connect();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        Toast.makeText(getApplicationContext(), "CONNECTED", Toast.LENGTH_LONG).show();

        Thread arduino = new Thread(new Runnable() {
            public void run() {
                //Пытаемся получить данные
                try {
                    //Получаем входной поток для получения данных
                    InputStream inpStream = clientSocket.getInputStream();
                    outputStream = clientSocket.getOutputStream();

                    String fileName = "Log.txt";

                    File file = createLogFile(fileName);

                    while (true) {

                        // прием данных ------
                        byte[] buffer = new byte[256];
                        int bytes = inpStream.read(buffer);
                        String data = new String(buffer, 0, bytes);

                        if ((data != null)&(data.length()>5)) {
                            System.out.println(data);
                            Double[] dataDouble = new Double[4];
                            dataDouble = parserDouble(data);
                            globalTemp = dataDouble[0];
                            globalHumidity =  dataDouble[1];
                            globalPressure = dataDouble[2];
                            globalHeight = dataDouble[3];
                            tempViewChanger.obtainMessage(1, tempChangeUnit(flagTemp, globalTemp)).sendToTarget();
                            humidityViewChanger.obtainMessage(1, humidityChangeUnit(flagHumidity, globalHumidity)).sendToTarget();
                            pressureViewChanger.obtainMessage(1, pressureChangeUnit(flagPressure, globalPressure)).sendToTarget();
                            heightViewChanger.obtainMessage(1, heightChangeUnit(flagHeight, globalHeight)).sendToTarget();
                        }

                        String message = createLogString();
                        writeLog(file, message);

                        Thread.sleep(30000);

                    }
                }catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        arduino.start();
    }

    private String createLogString () {
        String sTemp = flagTemp ? " Temp in °C: " : " Temp in °F: ";
        String sHumidity = flagHumidity ? " Humidity in %: " : " Humidity in words: ";
        String sPressure = flagPressure ? " Pressure in mm rt: " : " Pressure in Pa: " ;
        String sHeight = flagHeight ? " Height in m: " : " Height in sm: ";

        Date date = new Date();
        DateFormat targetFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        String dateStr = targetFormat.format(date) + ":";

        return dateStr + sTemp + tempChangeUnit(flagTemp, globalTemp) + sHumidity + humidityChangeUnit(flagHumidity, globalHumidity) + sPressure + pressureChangeUnit(flagPressure, globalPressure) + sHeight + heightChangeUnit(flagHeight, globalHeight) + "\n";

    }

    private File createLogFile(String fileName)
    {
        File externalAppDir = new File(Environment.getExternalStorageDirectory().toString());
        if (!externalAppDir.exists()) {
            externalAppDir.mkdir();
        }

        File file = new File(externalAppDir , fileName);
        try {
            file.createNewFile();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private boolean writeLog(File file, String message) {
        try {
            FileOutputStream outputStream = new FileOutputStream(file, true);
            outputStream.write(message.getBytes());
            outputStream.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;

    }

    private void setTime(){
        if (outputStream == null) {
            try {
                Date date = new Date();
                DateFormat targetFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                String outStr = targetFormat.format(date);
                byte[] bufferOut = new byte [128];

                bufferOut = outStr.getBytes();
                outputStream.write(bufferOut);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    private Double[] parserDouble (String str) {
        String[] strMas;
        strMas = str.split(" ");
        Double[] result = new Double[4];
        result[0] = Double.parseDouble(strMas[0]);
        result[1] = Double.parseDouble(strMas[1]);
        result[2] = Double.parseDouble(strMas[2]);
        result[3] = Double.parseDouble(strMas[3]);
        return result;
     }

    @Override
    public void onClick(View v) {

        if (v == codeTempLable)
        {
            flagTemp = !flagTemp;
            System.out.println(flagTemp);
            tempChanger.obtainMessage(1).sendToTarget();
            tempViewChanger.obtainMessage(1, tempChangeUnit(flagTemp, globalTemp)).sendToTarget();
        }
        if (v == codeHumidityLable)
        {
            flagHumidity = !flagHumidity;
            humidityChanger.obtainMessage(1).sendToTarget();
            humidityViewChanger.obtainMessage(1, humidityChangeUnit(flagHumidity, globalHumidity)).sendToTarget();
        }
        if (v == codePressureLable)
        {
            flagPressure = !flagPressure;
            pressureChanger.obtainMessage(1).sendToTarget();
            pressureViewChanger.obtainMessage(1, pressureChangeUnit(flagPressure, globalPressure)).sendToTarget();
        }
        if (v == codeHeightLable)
        {
            flagHeight = !flagHeight;
            heightChanger.obtainMessage(1).sendToTarget();
            heightViewChanger.obtainMessage(1, heightChangeUnit(flagHeight, globalHeight)).sendToTarget();
        }
        if (v == codeTimeButton)
        {
            if (outputStream != null){
                setTime();
            }
        }
    }
}
